package View;

import java.awt.Toolkit;
import javax.swing.JOptionPane;

public class TicketForm extends javax.swing.JInternalFrame {

    public TicketForm() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupo = new javax.swing.ButtonGroup();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        peli = new javax.swing.JComboBox<>();
        panel1 = new javax.swing.JPanel();
        dosd = new javax.swing.JRadioButton();
        tresd = new javax.swing.JRadioButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        precioadulto = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        precioniño = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        cantadulto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cantniño = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        totalapagar = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        vuelto = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        pagado = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();

        jLabel9.setText("C$");

        setClosable(true);

        jLabel1.setText("Película");

        peli.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Star Wars - El ascenso de Skywalker", "Spider Man - De Regreso A Casa", "La Pasion De Cristo" }));
        peli.setSelectedIndex(-1);
        peli.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                peliItemStateChanged(evt);
            }
        });
        peli.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                peliActionPerformed(evt);
            }
        });

        panel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Tipo de sala"));

        grupo.add(dosd);
        dosd.setText("2D");
        dosd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dosdActionPerformed(evt);
            }
        });

        grupo.add(tresd);
        tresd.setText("3D");
        tresd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tresdActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(dosd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tresd)
                .addGap(81, 81, 81))
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel1Layout.createSequentialGroup()
                .addGroup(panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dosd)
                    .addComponent(tresd))
                .addGap(0, 7, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Precio por boleto"));

        jLabel2.setText("Adultos");

        precioadulto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        precioadulto.setText("C$ 0");

        jLabel4.setText("Niños");

        precioniño.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        precioniño.setText("C$ 0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(precioadulto)
                .addGap(58, 58, 58)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(precioniño)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(precioadulto)
                    .addComponent(jLabel4)
                    .addComponent(precioniño))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Cantidad de boletos"));

        cantadulto.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantadulto.setText("0");
        cantadulto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cantadultoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantadultoKeyTyped(evt);
            }
        });

        jLabel6.setText("Adultos");

        jLabel7.setText("Niños");

        cantniño.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        cantniño.setText("0");
        cantniño.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                cantniñoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantniñoKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel6)
                .addGap(29, 29, 29)
                .addComponent(cantadulto, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(29, 29, 29)
                .addComponent(cantniño, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cantniño, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cantadulto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel8.setText("Total a pagar");

        totalapagar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        totalapagar.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalapagar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                totalapagarMouseReleased(evt);
            }
        });

        jLabel10.setText("Total pagado");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        vuelto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        vuelto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel13.setText("Cambio");

        pagado.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        pagado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pagadoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                pagadoKeyTyped(evt);
            }
        });

        jLabel3.setText("C$");

        jLabel5.setText("C$");

        jLabel14.setText("C$");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(peli, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 12, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel13))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel10)
                                    .addComponent(jLabel8))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel14))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(totalapagar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pagado, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                            .addComponent(vuelto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(9, 9, 9))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(peli, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totalapagar, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(pagado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5))
                    .addComponent(vuelto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(32, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void peliItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_peliItemStateChanged

    }//GEN-LAST:event_peliItemStateChanged

    private void peliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_peliActionPerformed
        String Pelicula;

        Pelicula = peli.getSelectedItem().toString();
        if (!Pelicula.equals("Star Wars - El ascenso de Skywalker")) {
            panel1.setEnabled(true);
            dosd.setEnabled(true);
            tresd.setEnabled(false);
        } else {
            panel1.setEnabled(true);
            dosd.setEnabled(true);
            tresd.setEnabled(true);
        }
    }//GEN-LAST:event_peliActionPerformed

    private void dosdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dosdActionPerformed

        precioadulto.setText("140");
        precioniño.setText("120");
    }//GEN-LAST:event_dosdActionPerformed

    private void tresdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tresdActionPerformed
        precioadulto.setText("180");
        precioniño.setText("150");
    }//GEN-LAST:event_tresdActionPerformed

    private void totalapagarMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_totalapagarMouseReleased
        int adulto;
        int niño;

        adulto = Integer.parseInt(cantadulto.getText()) * Integer.parseInt(precioadulto.getText());
        niño = Integer.parseInt(cantniño.getText()) * Integer.parseInt(precioniño.getText());

        totalapagar.setText(String.valueOf(adulto + niño));
    }//GEN-LAST:event_totalapagarMouseReleased

    private void cantadultoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantadultoKeyReleased
        int adulto;
        adulto = ((Integer.parseInt(cantadulto.getText()) * (Integer.parseInt(precioadulto.getText()))));
        totalapagar.setText(String.valueOf(adulto));
    }//GEN-LAST:event_cantadultoKeyReleased

    private void cantniñoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantniñoKeyReleased
        int niño;
        int adulto;
        adulto = Integer.parseInt(cantadulto.getText()) * Integer.parseInt(precioadulto.getText());
        niño = Integer.parseInt(cantniño.getText()) * Integer.parseInt(precioniño.getText());
        totalapagar.setText(String.valueOf(adulto + niño));
    }//GEN-LAST:event_cantniñoKeyReleased

    private void pagadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pagadoKeyReleased
        int cambio;
        int paga = Integer.parseInt(pagado.getText());
        int total = Integer.parseInt(totalapagar.getText());
        cambio = (Integer.parseInt(pagado.getText()) - Integer.parseInt(totalapagar.getText()));
        vuelto.setText(String.valueOf(cambio));
    }//GEN-LAST:event_pagadoKeyReleased

    private void cantadultoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantadultoKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_cantadultoKeyTyped

    private void cantniñoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantniñoKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_cantniñoKeyTyped

    private void pagadoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pagadoKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_pagadoKeyTyped


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField cantadulto;
    private javax.swing.JTextField cantniño;
    private javax.swing.JRadioButton dosd;
    private javax.swing.ButtonGroup grupo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField pagado;
    private javax.swing.JPanel panel1;
    private javax.swing.JComboBox<String> peli;
    private javax.swing.JLabel precioadulto;
    private javax.swing.JLabel precioniño;
    private javax.swing.JLabel totalapagar;
    private javax.swing.JRadioButton tresd;
    private javax.swing.JLabel vuelto;
    // End of variables declaration//GEN-END:variables
}
