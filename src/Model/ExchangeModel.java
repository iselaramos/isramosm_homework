package Model;

import java.io.Serializable;

public class ExchangeModel implements Serializable{
    Double NumberOne,NumberTwo;
    String CaseOne,CaseTwo;

    public String getCaseOne() {
        return CaseOne;
    }

    public void setCaseOne(String CaseOne) {
        this.CaseOne = CaseOne;
    }

    public String getCaseTwo() {
        return CaseTwo;
    }

    public void setCaseTwo(String CaseTwo) {
        this.CaseTwo = CaseTwo;
    }

    public Double getNumberOne() {
        return NumberOne;
    }

    public void setNumberOne(Double NumberOne) {
        this.NumberOne = NumberOne;
    }

    public Double getNumberTwo() {
        return NumberTwo;
    }

    public void setNumberTwo(Double NumberTwo) {
        this.NumberTwo = NumberTwo;
    }
    
    
}
