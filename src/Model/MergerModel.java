package Model;

import java.io.Serializable;

public class MergerModel implements Serializable{
    String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
