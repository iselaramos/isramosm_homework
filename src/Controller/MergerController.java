package Controller;

import Model.MergerModel;
import View.Merger;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

public class MergerController implements ActionListener {

    Merger M;
    MergerModel MM;
    JFileChooser d;

    public MergerController(Merger M) {
        super();
        this.M = M;
        MM = new MergerModel();
        d = new JFileChooser();
    }

    public MergerModel getMM() {
        return MM;
    }

    public void setMM(MergerModel MM) {
        this.MM = MM;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "OpenOne":
                OpenOne();

                break;
            case "OpenTwo":
                OpenTwo();
                break;
            case "Save":
                Save();
                break;
            case "Clean":
                M.Clean();
                break;
        }
    }

    public void OpenOne() {
        String txtruta = null;
        try {
            JFileChooser jf = new JFileChooser();
            jf.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter fil = new FileNameExtensionFilter(".txt", "txt", "text");
            jf.setFileFilter(fil);
            int el = jf.showOpenDialog(M);
            if (el == JFileChooser.APPROVE_OPTION) {
                txtruta = (jf.getSelectedFile().getAbsolutePath());
            }
            File ruta = new File(txtruta);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            M.RutaOne.setText(txtruta);
            String linea = null;
            while ((linea = bu.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(linea, "");
                String cadena = M.editorPane.getText();
                M.editorPane.setText(cadena + " " + st.nextToken());
            }
            bu.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void OpenTwo() {
        String txtruta = null;
        try {
            JFileChooser jf = new JFileChooser();
            jf.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter fil = new FileNameExtensionFilter(".txt", "txt", "text");
            jf.setFileFilter(fil);
            jf.setCurrentDirectory(new File("Fotos"));
            int el = jf.showOpenDialog(M);
            if (el == JFileChooser.APPROVE_OPTION) {

                txtruta = (jf.getSelectedFile().getAbsolutePath());
            }
            File ruta = new File(txtruta);
            FileReader fi = new FileReader(ruta);
            BufferedReader bu = new BufferedReader(fi);
            M.RutaTwo.setText(txtruta);
            String linea = null;
            while ((linea = bu.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(linea, "");
                String cadena = M.editorPane.getText();
                M.editorPane.setText(cadena + " " + st.nextToken());
            }
            bu.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void Save() {
        FileWriter fichero = null;
        PrintWriter pw = null;
        MM = M.getMergerData();
        try {
            String txtruta = null;
            JFileChooser jf = new JFileChooser();
            jf.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter fil = new FileNameExtensionFilter(".txt", "txt", "text");
            jf.setFileFilter(fil);
            jf.setCurrentDirectory(new File("Fotos"));
            int el = jf.showSaveDialog(M);
            if (el == JFileChooser.APPROVE_OPTION) {

                txtruta = (jf.getSelectedFile().getAbsolutePath());
            }
            fichero = new FileWriter(txtruta + ".txt");
            pw = new PrintWriter(fichero);

            pw.println(MM.getText());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
