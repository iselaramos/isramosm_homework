package Controller;

import Model.ExchangeModel;
import View.ExchangeRateForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ExchangeController implements ActionListener{
    ExchangeRateForm E;
    ExchangeModel EM;
    
    public ExchangeController(ExchangeRateForm E){
        super();
        this.E = E;
        EM = new ExchangeModel();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "Converter":
                convert();
                break;
                
            case "Clean":
                E.clean();
                break;
        }
    }
    
    public void convert(){
        EM = E.getMoneyData();
        switch(EM.getCaseOne()){
            case "Cordobas":
                if(EM.getCaseTwo().equals("Cordobas")){
                    EM.setNumberTwo(EM.getNumberOne()*1);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Dolares")){
                    EM.setNumberTwo(EM.getNumberOne()*35);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Euros")){
                    EM.setNumberTwo(EM.getNumberOne()*40);      
                    E.setMoneyData(EM);
                }
                break;
            case "Dolares":
                if(EM.getCaseTwo().equals("Cordobas")){
                    EM.setNumberTwo(EM.getNumberOne()*35);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Dolares")){
                    EM.setNumberTwo(EM.getNumberOne()*1);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Euros")){
                    EM.setNumberTwo(EM.getNumberOne()*0.83);      
                    E.setMoneyData(EM);
                }
                break;
                
            case "Euros":
                if(EM.getCaseTwo().equals("Cordobas")){
                    EM.setNumberTwo(EM.getNumberOne()*42.30);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Dolares")){
                    EM.setNumberTwo(EM.getNumberOne()*0.83);      
                    E.setMoneyData(EM);
                }else if(EM.getCaseTwo().equals("Euros")){
                    EM.setNumberTwo(EM.getNumberOne()*1);      
                    E.setMoneyData(EM);
                }
                break;
        }
    }
}
