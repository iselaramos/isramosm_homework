package Misc;

import javax.swing.JComboBox;

public class Departamentos {

    public static void desparts(JComboBox DepComboBox) {
        DepComboBox.addItem("Managua");
        DepComboBox.addItem("Boaco");
        DepComboBox.addItem("Carazo");
        DepComboBox.addItem("Chinandega");
        DepComboBox.addItem("Chontales");
        DepComboBox.addItem("Costa Caribe Norte");
        DepComboBox.addItem("Costa Caribe Sur");
        DepComboBox.addItem("Esteli");
        DepComboBox.addItem("Granada");
        DepComboBox.addItem("Jinitega");
        DepComboBox.addItem("Leon");
        DepComboBox.addItem("Madriz");
        DepComboBox.addItem("Masaya");
        DepComboBox.addItem("Matagalpa");
        DepComboBox.addItem("Nueva segovia");
        DepComboBox.addItem("Rios san juan");
        DepComboBox.addItem("Rivas");

    }

    public static void municipios(JComboBox DepComboBox, JComboBox MunComboBox) {
        MunComboBox.removeAllItems();
        switch (DepComboBox.getSelectedItem().toString()) {

            case "Managua":
                MunComboBox.addItem("Ciudad sandino");
                MunComboBox.addItem("Tipitapa");
                MunComboBox.addItem("El crucero");
                MunComboBox.addItem("Mateare");
                MunComboBox.addItem("San Francisco Libre");
                MunComboBox.addItem("San Rafael del Sur");
                MunComboBox.addItem("Ticuantepe");
                MunComboBox.addItem("villa el carmen");
                MunComboBox.addItem("Managua");

                break;

            case "Boaco":

                MunComboBox.addItem("Boaco");
                MunComboBox.addItem("Camoapa");
                MunComboBox.addItem("San jose de los Remates");
                MunComboBox.addItem("San lorenzo");
                MunComboBox.addItem("Santa lucia");
                MunComboBox.addItem("Teustepe");

                break;

            case "Carazo":

                MunComboBox.addItem("Diriamba");
                MunComboBox.addItem("Dolores");
                MunComboBox.addItem("El Rosario");
                MunComboBox.addItem("Jinotepe");
                MunComboBox.addItem("La Conquista");
                MunComboBox.addItem("La Paz de Oriente");
                MunComboBox.addItem("San Marcos");
                MunComboBox.addItem("Santa Teresa");

                break;

            case "Chinandega":
                MunComboBox.addItem("Chichigalpa");
                MunComboBox.addItem("Chinandega");
                MunComboBox.addItem("Cinco Pinos");
                MunComboBox.addItem("Corinto");
                MunComboBox.addItem("El Realejo");
                MunComboBox.addItem("El Viejo");
                MunComboBox.addItem("Posoltega");
                MunComboBox.addItem("Tonalá");
                MunComboBox.addItem("San Francisco del Norte");
                MunComboBox.addItem("San Pedro del Norte");
                MunComboBox.addItem("Santo Tomás del Norte");
                MunComboBox.addItem("Somotillo");
                MunComboBox.addItem("Villanueva");

                break;

            case "Chontales":

                MunComboBox.addItem("Acoyapa");
                MunComboBox.addItem("Comalapa");
                MunComboBox.addItem("Cuapa");
                MunComboBox.addItem("El Coral");
                MunComboBox.addItem("Juigalpa ");
                MunComboBox.addItem("La Libertad");
                MunComboBox.addItem("San Pedro de Lóvago");
                MunComboBox.addItem("Santo Domingo");
                MunComboBox.addItem("Santo Tomás");
                MunComboBox.addItem("Villa Sandino");

                break;

            case "Costa Caribe Norte":
                MunComboBox.addItem("Bonanza");
                MunComboBox.addItem("Mulukukú");
                MunComboBox.addItem("Prinzapolka");
                MunComboBox.addItem("Puerto Cabeza");
                MunComboBox.addItem("Rosita");
                MunComboBox.addItem("Siuna");
                MunComboBox.addItem("Waslala");
                MunComboBox.addItem("Waspán");

                break;

            case "Costa Caribe Sur":
                MunComboBox.addItem("Bluefields ");
                MunComboBox.addItem("Desembocadura de Río Grande");
                MunComboBox.addItem("El Ayote");
                MunComboBox.addItem("El Rama");
                MunComboBox.addItem("El Tortuguero");
                MunComboBox.addItem("Islas del Maíz");
                MunComboBox.addItem("Kukra Hill");
                MunComboBox.addItem("La Cruz de Río Grande");
                MunComboBox.addItem("Laguna de Perlas");
                MunComboBox.addItem("Muelle de los Bueyes");
                MunComboBox.addItem("Nueva Guinea");
                MunComboBox.addItem("Bocana de Paiwas");

                break;

            case "Esteli":

                MunComboBox.addItem(" Condega");
                MunComboBox.addItem(" Estelí ");
                MunComboBox.addItem(" La Trinidad");
                MunComboBox.addItem("Pueblo Nuevo ");
                MunComboBox.addItem(" San Juan de Limay");
                MunComboBox.addItem(" San Nicolás");

                break;

            case "Granada":

                MunComboBox.addItem("Diriá");
                MunComboBox.addItem("Diriomo");
                MunComboBox.addItem("Granada ");
                MunComboBox.addItem("Nandaime");

                break;

            case "Jinotega":

                MunComboBox.addItem("El Cuá");
                MunComboBox.addItem("Jinotega ");
                MunComboBox.addItem("La Concordia");
                MunComboBox.addItem("Bocay");
                MunComboBox.addItem("San Rafael del Norte");
                MunComboBox.addItem("Yalí");
                MunComboBox.addItem("Las Praderas");
                MunComboBox.addItem("Wiwilí de Jinotega");

                break;

            case "Leon":
                MunComboBox.addItem("Achuapa");
                MunComboBox.addItem("El Jicaral");
                MunComboBox.addItem("El Sauce");
                MunComboBox.addItem("La Paz Centro");
                MunComboBox.addItem("Larreynaga");
                MunComboBox.addItem("León ");
                MunComboBox.addItem("Nagarote");
                MunComboBox.addItem("Quezalguaque");
                MunComboBox.addItem("Santa Rosa del Peñón");
                MunComboBox.addItem("Telica");

                break;

            case "Madriz":

                MunComboBox.addItem("Las Sabanas");
                MunComboBox.addItem("Palacagüina");
                MunComboBox.addItem("San José de Cusmapa");
                MunComboBox.addItem("San Juan de Río Coco");
                MunComboBox.addItem("San Lucas");
                MunComboBox.addItem("Somoto ");
                MunComboBox.addItem("Telpaneca");
                MunComboBox.addItem("Totogalpa");
                MunComboBox.addItem("Yalagüina");

                break;

            case "Masaya":
                MunComboBox.addItem("Catarina");
                MunComboBox.addItem("La Concepción");
                MunComboBox.addItem("Masatepe");
                MunComboBox.addItem("Masaya ");
                MunComboBox.addItem("Nandasmo");
                MunComboBox.addItem("Nindirí");
                MunComboBox.addItem("Niquinohomo");
                MunComboBox.addItem("San Juan de Oriente");
                MunComboBox.addItem("Tisma");

                break;

            case "Matagalpa":
                MunComboBox.addItem("Ciudad Darío");
                MunComboBox.addItem("Esquipulas");
                MunComboBox.addItem("La Dalia");
                MunComboBox.addItem("Matagalpa");
                MunComboBox.addItem("Matiguás");
                MunComboBox.addItem("Muy Muy");
                MunComboBox.addItem("Rancho Grande");
                MunComboBox.addItem("Río Blanco");
                MunComboBox.addItem("San Dionisio");
                MunComboBox.addItem("San Isidro");
                MunComboBox.addItem("San Ramón");
                MunComboBox.addItem("Sébaco");
                MunComboBox.addItem("Terrabona");

                break;

            case "Nueva Segovia":
                MunComboBox.addItem(" Ciudad Antigua");
                MunComboBox.addItem("Dipilto");
                MunComboBox.addItem("El Jícaro");
                MunComboBox.addItem("Jalapa");
                MunComboBox.addItem("Macuelizo");
                MunComboBox.addItem("Mozonte");
                MunComboBox.addItem("Murra");
                MunComboBox.addItem("Ocotal");
                MunComboBox.addItem("Quilalí");
                MunComboBox.addItem("San Fernando");
                MunComboBox.addItem("Santa María");
                MunComboBox.addItem("Wiwilí de Nueva Segovia");

                break;

            case "Río San Juan":

                MunComboBox.addItem("El Almendro");
                MunComboBox.addItem("El Castillo");
                MunComboBox.addItem("Morrito");
                MunComboBox.addItem("San Carlos ");
                MunComboBox.addItem("San Juan del Norte");
                MunComboBox.addItem("San Miguelito");

                break;

            case "Rivas":

                MunComboBox.addItem("Altagracia");
                MunComboBox.addItem("Belén");
                MunComboBox.addItem("Buenos Aires");
                MunComboBox.addItem("Cárdenas ");
                MunComboBox.addItem("Potosí");
                MunComboBox.addItem("Moyogalpa");
                MunComboBox.addItem("Rivas ");
                MunComboBox.addItem("San Jorge");
                MunComboBox.addItem("Buenos Aires");
                MunComboBox.addItem("Tola");

                break;

            default:
        }

    }

}
